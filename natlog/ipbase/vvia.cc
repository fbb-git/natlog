#include "ipbase.ih"

void IPbase::vVia(Record const &record) const
{
    d_stdMsg << " (via: " << record.viaIPstr()  << ',' << 
                             showLeft(record.viaPort(), ')');
}
