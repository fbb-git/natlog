#include "ipbase.ih"

void IPbase::dstData(Record const &record) const
{
    d_logDataStream <<
        setw(11) << ntohl(record.destIP())  << ',' <<   // see also
        setw(16) << record.destIPstr()      << ',' <<   // logDataNoDst
        setw(8)  << record.destPort()       << ',';
}

