#include "ipbase.ih"

// static
string IPbase::showLeft(size_t port, char sep)
{
    string ret{ to_string(port) };
    size_t length = 5 - ret.length();
    ret += sep;
    ret.append(length, ' ');            // the field contains 12345;
                                        // (or shorter) and must be 6
                                        // characters wide. Called from 
                                        // logConnection.

    return ret;
}
