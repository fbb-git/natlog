#include "rotatingstreambuf.ih"

RotatingStreambuf::RotationInfo RotatingStreambuf::s_stdRotate;
RotatingStreambuf::RotationInfo RotatingStreambuf::s_dataRotate;

Semaphore RotatingStreambuf::s_semaphore{ 0 };
thread RotatingStreambuf::s_rotateThread;
