#include "conntrackproducer.ih"

void ConntrackProducer::signalHandler(size_t signum)
{
    d_stdMsg << "received signal " << signum << " (" << 
        (signum == SIGINT ? "SIGINT" : "SIGTERM") << ')' << endl;

    d_signaled = true;

    kill(pid(), SIGTERM);
    kill(pid(), SIGTERM);
    kill(pid(), SIGKILL);
}
