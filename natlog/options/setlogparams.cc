#include "options.ih"

void Options::setLogParams()
{
    // at construction time: d_rotateFreq = 0;

    d_arg.option(&d_logData, "log-data");                   // maybe log
                                                            // table data

    if (not d_arg.option(&d_log, "log"))     // no log spec: use syslog
    {
        d_log = "syslog";

        if (not d_arg.option(&d_syslogTag, "syslog-tag"))   // specific or 
            d_syslogTag = s_defaultSyslogIdent;             // default log-tag
    
        setSyslogFacility();
        setSyslogPriority();

        return;
    }
                                                // log specification indicates
    if (d_log == "off")                         // no logging
    {
        d_log.clear();
        return;
    }

    // path to the log-file was specified

    string value;
    if (d_arg.option(&value, "log-rotate"))     // log file rotation in days
    {
        istringstream in{ value };

        in >> d_rotateFreq;
        switch (in.get())
        {
            case 'd':
                d_rotateFactor = 24 * 60;
                d_rotateTimeSpec = " days";
            break;

            case 'h':
                d_rotateFactor = 60;
                d_rotateTimeSpec = " hours";
            break;

            case 'm':
                d_rotateTimeSpec = " minutes";
            break;

            default:
            throw Exception{} << "`log-rotate " << value << "' not supported";
        }

        if (d_rotateFreq == 1)
            d_rotateTimeSpec.pop_back();

        if (isdigit(in.peek()))
        {
            in >> d_nRotations;
            if (d_nRotations == 0)
                d_nRotations = 1;
        }
        else if (in.peek() == EOF)
            d_nRotations = 1;
        else
            throw Exception{} << "`--log-rotate " << value << 
                                                    "' not supported";
    }
}







