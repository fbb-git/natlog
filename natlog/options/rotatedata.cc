#include "options.ih"

bool Options::rotateData() const
{
    if (d_rotateData)
        ::kill(readPid(), SIGALRM);

    return d_rotateData;
}
